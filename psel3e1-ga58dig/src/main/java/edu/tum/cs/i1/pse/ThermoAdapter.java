package edu.tum.cs.i1.pse;

import edu.tum.cs.i1.thermometers.FahrenheitThermo;

public class ThermoAdapter implements ThermoInterface{

	private FahrenheitThermo fahrenheitThermo;
	
	public ThermoAdapter() {
		fahrenheitThermo = new FahrenheitThermo();
	}
	
	public double getTempC() {
		return (fahrenheitThermo.getFarenheitTemperature() - 32) * 5/9;
	}

}
