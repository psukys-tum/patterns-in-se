package edu.tum.cs.i1.pse;

import java.util.Arrays;

public class Firewall implements ConnectionInterface {
	
	private NetworkConnection realConnection;
	private int[] allowedPorts;
	
	public Firewall(int[] ap) {
		allowedPorts = ap;
		realConnection = new NetworkConnection();
	}
	
	private boolean isAllowed(int port) {
		boolean allowed = false;
		
		for (int ap: allowedPorts) {
			if (ap == port) {
				allowed = true;
				break;
			}
		}
		
		return allowed;
	}
	
	public void open(String addr, int port) {
		if (isAllowed(port)) {
			System.out.println("Connection established");
			realConnection.open(addr, port);
		} else {
			throw new RuntimeException("Connection rejected!");
		}
	}
	
	public void close() {
		realConnection.close();
	}
	
	public boolean isConnected() {
		return realConnection.isConnected();
	}
	
	public int[] getAllowedPorts() {
		return allowedPorts;
	}
}
