package de.tum.cs.i1.pse.blackboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.tum.cs.i1.pse.knowledgeSource.KnowledgeSource;

import edu.stanford.nlp.ling.TaggedWord;


public class BlackBoard {

	private BlackBoardState state = BlackBoardState.CLEAN;
	private List<KnowledgeSource> knowledgeSources = new ArrayList<KnowledgeSource>();

	private String inputSentence;
	private List<TaggedWord> taggedValues = new ArrayList<TaggedWord>();
	private String synonymSentence;
	private String encryptedSentence;

	public List<KnowledgeSource> getKnowledgeSources() {
		if (knowledgeSources == null) {
			knowledgeSources = new ArrayList<KnowledgeSource>();
		}
		return Collections.unmodifiableList(knowledgeSources);
	}

	public void addKnowledgeSource(KnowledgeSource ks) {
		if (knowledgeSources == null) {
			knowledgeSources = new ArrayList<KnowledgeSource>();
		}
		knowledgeSources.add(ks);
	}

	public void clean() {
		this.inputSentence = null;
		this.taggedValues.clear();
		this.synonymSentence = null;
		this.encryptedSentence = null;
		this.state = BlackBoardState.CLEAN;
	}

	public BlackBoardState getState() {
		return state;
	}

	public void setStateDone() {
		this.state = BlackBoardState.DONE;
	}

	public String getInputSentence() {
		return inputSentence;
	}

	public void setInputSentence(String inputSentence) {
		this.inputSentence = inputSentence;
		this.state = BlackBoardState.INPUT_SENTENCE_SET;
	}

	public void setTaggedValues(List<TaggedWord> taggedValues) {
		this.taggedValues = taggedValues;
		this.state = BlackBoardState.TAGGED_VALUES_SET;
	}

	public List<TaggedWord> getTaggedValues() {
		return Collections.unmodifiableList(taggedValues);
	}

	public void setSynonymSentence(String synonymSentence) {
		this.synonymSentence = synonymSentence;
		this.state = BlackBoardState.SYNONYM_SENTENCE_SET;
	}

	public String getSynonymSentence() {
		return this.synonymSentence;
	}

	public void setEncryptedSentence(String encryptedSentence) {
		this.encryptedSentence = encryptedSentence;
		this.state = BlackBoardState.ENCRYPTED_SENTENCE_SET;
	}

	public String getEncryptedSentence() {
		return this.encryptedSentence;
	}
}
