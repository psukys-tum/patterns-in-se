package de.tum.cs.i1.pse.knowledgeSource;

import java.util.List;

import de.tum.cs.i1.pse.blackboard.BlackBoard;
import de.tum.cs.i1.pse.blackboard.BlackBoardState;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;


public class PartOfSpeechHypothesizer extends KnowledgeSource {
	
	private MaxentTagger tagger;

	public PartOfSpeechHypothesizer(BlackBoard blackboard, MaxentTagger maxentTagger) {
		this.blackBoard = blackboard;
		this.tagger = maxentTagger;
	}

	private String[] splitSentence(String sentence) {
		return sentence.split(" ");
	}

	private List<TaggedWord> extractPoS(String[] splitSentence) {
		List<HasWord> wordList = Sentence.toWordList(splitSentence);
		List<TaggedWord> taggedSentence = tagger.tagSentence(wordList);
		return taggedSentence;
	}

	@Override
	public boolean executeCondition() {
		if (this.blackBoard.getState() == BlackBoardState.INPUT_SENTENCE_SET) {
			// This knowledge source should take action only if there is raw
			// text is on the BlackBoard
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void executeAction() {
		updateBlackBoard();
	}

	@Override
	protected void updateBlackBoard() {
		String rawSentence = this.blackBoard.getInputSentence();
		if (rawSentence != null) {
			String[] splitSentenc = splitSentence(rawSentence);
			this.blackBoard.setTaggedValues(extractPoS(splitSentenc));
		}
	}
}
