package de.tum.cs.i1.pse.knowledgeSource;

import java.util.List;

import org.jasypt.util.text.BasicTextEncryptor;

import de.tum.cs.i1.pse.blackboard.BlackBoard;
import de.tum.cs.i1.pse.blackboard.BlackBoardState;

import edu.stanford.nlp.ling.TaggedWord;


public class PrivacyProtector extends KnowledgeSource {
	private String encryptionKey = "SXGWOwgzldpLZPDkoHell0ifWrlduJYvjhtysecureFrOmN$A.ACE";
	private BlackBoard blackBoard;
	
	public PrivacyProtector(BlackBoard blackBoard) {
		this.blackBoard = blackBoard;
	}
	
	public boolean executeCondition() {
		return blackBoard.getState() == BlackBoardState.SYNONYM_SENTENCE_SET && isEncryptionRequired();
	}

	public void executeAction() {
		updateBlackBoard();
	}

	protected void updateBlackBoard() {
		blackBoard.setEncryptedSentence(encrypt(blackBoard.getSynonymSentence()));
	}

    private boolean isEncryptionRequired() {
        List<TaggedWord> taggedValues = this.blackBoard.getTaggedValues();
        if (taggedValues != null && !taggedValues.isEmpty()) {
            for (TaggedWord word : taggedValues) {
                if (word.tag().contains("NNP") || word.tag().contains("NNPS")) {
                    return true;
                }
            }
        }
        return false;
    }

	private String encrypt(String plainText) {
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword(encryptionKey);
		return textEncryptor.encrypt(plainText);
	}
}
