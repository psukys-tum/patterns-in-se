package de.tum.cs.i1.pse.blackboard;

public enum BlackBoardState {
	CLEAN, 
	INPUT_SENTENCE_SET,
	TAGGED_VALUES_SET,
	SYNONYM_SENTENCE_SET,
	ENCRYPTED_SENTENCE_SET,
	DONE
}
