package de.tum.cs.i1.pse.control;

import java.util.ArrayList;

import de.tum.cs.i1.pse.blackboard.BlackBoard;
import de.tum.cs.i1.pse.blackboard.BlackBoardState;
import de.tum.cs.i1.pse.knowledgeSource.KnowledgeSource;


public class Control {

	private BlackBoard blackBoard;
	private InputSource inputSource;

	public Control(final BlackBoard blackBoard) {
		this.blackBoard = blackBoard;
		this.inputSource = new InputSource();
	}

	public void start() {
		for (String sentence : inputSource.getSentences()) {
			this.blackBoard.clean();
			this.blackBoard.setInputSentence(sentence);
			applyKnowledgeSources();
			System.out.println("\nOriginal sentence: " + sentence);
			System.out.println("Synonym sentence: " + this.blackBoard.getSynonymSentence());
			System.out.println("Encrypted sentence: " + this.blackBoard.getEncryptedSentence() + "\n");
		}
	}

	public void applyKnowledgeSources() {
		while (blackBoard.getState() != BlackBoardState.DONE) {
			ArrayList<KnowledgeSource> candidateKnowledgeSources = new ArrayList<KnowledgeSource>();
			for (KnowledgeSource knowledgeSource : this.blackBoard.getKnowledgeSources()) {
				if (knowledgeSource.executeCondition()) {
					candidateKnowledgeSources.add(knowledgeSource);
				}				
			}

			
			if (candidateKnowledgeSources.isEmpty()) {
				this.blackBoard.setStateDone();
			} else {
				for (KnowledgeSource knowledgeSource: candidateKnowledgeSources) {
					knowledgeSource.executeAction();
				}
			}
			
			
			
			
		}
	}
}
