package de.tum.cs.i1.pse.control;

import java.util.ArrayList;
import java.util.List;


public class InputSource {
	private static final String SOFTWARES_CAN_ANALYZE = "Softwares can analyze, understand, and generate languages that humans use naturally";
	private static final String GOOGLE_OCCASIONALLY = "Google occasionally produces weird translation of the german language input";
	private static final String THE_QUICK_BROWN_FOX = "The quick brown fox jumps over the lazy dog";
	private static final String BOB_S_PHONE = "Bob's phone was intercepted by the NSA";
	private static final String HOW_ARE_YOU = "How are you today?";
	private List<String> sentences;

	public InputSource() {
		sentences = new ArrayList<String>();
		sentences.add(SOFTWARES_CAN_ANALYZE);
		sentences.add(GOOGLE_OCCASIONALLY);
		sentences.add(THE_QUICK_BROWN_FOX);
		sentences.add(BOB_S_PHONE);
		sentences.add(HOW_ARE_YOU);
	}

	public List<String> getSentences() {
		return sentences;
	}
	
	/** Added for tests **/
	public static String getHowAreYou() {
		return HOW_ARE_YOU;
	}
	
	/** Added for tests **/
	public static String getBob() {
		return BOB_S_PHONE;
	}
}
