package de.tum.cs.i1.pse.knowledgeSource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.tum.cs.i1.pse.blackboard.BlackBoard;
import de.tum.cs.i1.pse.blackboard.BlackBoardState;

import edu.stanford.nlp.ling.TaggedWord;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.data.Pointer;
import net.didion.jwnl.data.PointerTarget;
import net.didion.jwnl.data.PointerType;
import net.didion.jwnl.data.PointerUtils;
import net.didion.jwnl.data.Synset;
import net.didion.jwnl.data.Word;
import net.didion.jwnl.data.list.PointerTargetTree;
import net.didion.jwnl.dictionary.Dictionary;


public class SynonymIdentifier extends KnowledgeSource {

	private String propsFile = "conf/properties_file.xml";
	private Random random = new Random();

	private Map<String, String> replacementMap = new HashMap<String, String>();

	public SynonymIdentifier(BlackBoard blackBoard) {
		try {
			JWNL.initialize(new FileInputStream(propsFile));
		} catch (JWNLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		this.blackBoard = blackBoard;
	}

	@Override
	public boolean executeCondition() {
		return this.blackBoard.getState() == BlackBoardState.TAGGED_VALUES_SET;
	}

	@Override
	public void executeAction() {
		updateBlackBoard();
	}

	@Override
	protected void updateBlackBoard() {
		List<TaggedWord> taggedValues = this.blackBoard.getTaggedValues();
		if (taggedValues != null) {
			Iterator<TaggedWord> iterator = taggedValues.iterator();
			while (iterator.hasNext()) {
				try {
					findSynonyms(iterator.next());
				} catch (JWNLException e) {
					e.printStackTrace();
				}
			}
		}

		String synonymSentence = generateSynonymSentence();
		this.blackBoard.setSynonymSentence(synonymSentence);
	}

	private void findSynonyms(TaggedWord taggedWord) throws JWNLException {
		String tag = taggedWord.tag();
		String value = taggedWord.value();
		if (tag.contains("NN")) {
			findSynonym(POS.NOUN, value);
		} else if (tag.contains("VB")) {
			findSynonym(POS.VERB, value);
		} else if (tag.contains("JJ")) {
			findSynonym(POS.ADJECTIVE, value);
		}
	}

	private void findSynonym(POS pos, String value) throws JWNLException {
		IndexWord indexWord = Dictionary.getInstance().getIndexWord(pos, value);
		if (indexWord != null) {
			ArrayList<String> synonyms = new ArrayList<String>();
			synonyms.addAll(findSynonyms(indexWord, PointerType.SIMILAR_TO));
			synonyms.remove(value);
			if (!synonyms.isEmpty()) {
				replacementMap.put(value, getRandomSynonym(synonyms));
			}
		}
	}

	private List<String> findSynonyms(IndexWord w, PointerType type) throws JWNLException {
		List<Synset> synsets = new ArrayList<Synset>();
		List<String> synonyms = new ArrayList<String>();
		Synset[] senses = w.getSenses();
		
		for (int i = 0; i < senses.length; i++) {
			List<Synset> relatedSynsets = getRelatedSynsets(senses[i], type);
			if (relatedSynsets != null) {
				synsets.addAll(relatedSynsets);
			}
		}
		
		if (!synsets.isEmpty()) {
			for (int i = 0; i < synsets.size(); i++) {
				Synset s = (Synset) synsets.get(i);
				Word[] words = s.getWords();
				for (int j = 0; j < words.length; j++) {
					synonyms.add(words[j].getLemma());
				}
			}
		}
		
		return synonyms;
	}

	private List<Synset> getRelatedSynsets(Synset sense, PointerType type) throws JWNLException, NullPointerException {
		PointerTargetTree relatedList;
		relatedList = PointerUtils.getInstance().getHypernymTree(sense);
		PointerTarget target = relatedList.getRootNode().getPointerTarget();
		ArrayList<Synset> a = new ArrayList<Synset>();
		Pointer[] pointers = target.getPointers();
		
		for (int i = 0; i < pointers.length; i++) {
			a.add(pointers[i].getTargetSynset());
		}

		return a;
	}

	private String getRandomSynonym(List<String> synonyms) {
		int index = random.nextInt(synonyms.size());
		return synonyms.get(index);
	}

	private String generateSynonymSentence() {
		String[] split = this.blackBoard.getInputSentence().split(" ");
		StringBuffer buffer = new StringBuffer();
		
		for (String s : split) {
			String string = replacementMap.get(s);
			if (string != null) {
				buffer.append(string);
			} else {
				buffer.append(s);
			}
			buffer.append(" ");
		}
		
		return buffer.toString();
	}
}