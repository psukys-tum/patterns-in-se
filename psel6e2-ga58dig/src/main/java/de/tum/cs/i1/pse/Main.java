package de.tum.cs.i1.pse;

import java.io.IOException;

import de.tum.cs.i1.pse.blackboard.BlackBoard;
import de.tum.cs.i1.pse.control.Control;
import de.tum.cs.i1.pse.knowledgeSource.PartOfSpeechHypothesizer;
import de.tum.cs.i1.pse.knowledgeSource.PrivacyProtector;
import de.tum.cs.i1.pse.knowledgeSource.SynonymIdentifier;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;


public class Main {
	public static final String TAGGER_RESOURCE = "conf/english-left3words-distsim.tagger";

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		BlackBoard blackBoard = new BlackBoard();

		blackBoard.addKnowledgeSource(new SynonymIdentifier(blackBoard));
		MaxentTagger posTagger = new MaxentTagger(TAGGER_RESOURCE);
		blackBoard.addKnowledgeSource(new PartOfSpeechHypothesizer(blackBoard, posTagger));
		
		blackBoard.addKnowledgeSource(new PrivacyProtector(blackBoard));
		
		
		

		Control control = new Control(blackBoard);
		control.start();
	}
}
