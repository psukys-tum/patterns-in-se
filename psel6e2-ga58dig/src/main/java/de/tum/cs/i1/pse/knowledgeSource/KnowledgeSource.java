package de.tum.cs.i1.pse.knowledgeSource;

import de.tum.cs.i1.pse.blackboard.BlackBoard;


public abstract class KnowledgeSource {
	
	protected BlackBoard blackBoard;

	protected abstract void updateBlackBoard();
	public abstract boolean executeCondition();
	public abstract void executeAction();
}
