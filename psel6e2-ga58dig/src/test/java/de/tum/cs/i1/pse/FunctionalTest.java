package de.tum.cs.i1.pse;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import de.tum.cs.i1.pse.blackboard.BlackBoard;
import de.tum.cs.i1.pse.control.Control;
import de.tum.cs.i1.pse.control.InputSource;
import de.tum.cs.i1.pse.knowledgeSource.PartOfSpeechHypothesizer;
import de.tum.cs.i1.pse.knowledgeSource.PrivacyProtector;
import de.tum.cs.i1.pse.knowledgeSource.SynonymIdentifier;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;


public class FunctionalTest {

	@Test
	public void testIfPrivacyProtectorEncrypts() throws ClassNotFoundException, IOException {
		BlackBoard blackBoard = initBlackboard(InputSource.getBob());
		assertNotNull(blackBoard.getEncryptedSentence());
	}
	
	@Test
	public void testIfPrivacyProtectorDoesNotEncrypt() throws ClassNotFoundException, IOException {
		BlackBoard blackBoard = initBlackboard(InputSource.getHowAreYou());
		assertNull(blackBoard.getEncryptedSentence());
	}

	private BlackBoard initBlackboard(String inputSentence) throws ClassNotFoundException, IOException {
		BlackBoard blackBoard = new BlackBoard();

		blackBoard.addKnowledgeSource(new SynonymIdentifier(blackBoard));
		blackBoard.addKnowledgeSource(new PartOfSpeechHypothesizer(blackBoard, new MaxentTagger(Main.TAGGER_RESOURCE)));
		blackBoard.addKnowledgeSource(new PrivacyProtector(blackBoard));

		Control control = new Control(blackBoard);
		blackBoard.clean();
		blackBoard.setInputSentence(inputSentence);
		control.applyKnowledgeSources();
		return blackBoard;
	}

}
