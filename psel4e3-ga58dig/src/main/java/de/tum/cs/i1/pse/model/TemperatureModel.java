package de.tum.cs.i1.pse.model;

public class TemperatureModel extends java.util.Observable {

	private double temperatureC = 0.0;
	private double kelvin = 273.15;

	public double getF() {
		return (temperatureC * 9.0 / 5.0) + 32.0;
	}

	public double getC() {
		return temperatureC;
	}
	
	public double getK() {
		return kelvin + temperatureC;
	}

	public void setK(double tempK) {
		this.temperatureC = tempK - kelvin;
		setChanged();
		notifyObservers();
	}
	
	public void setF(double tempF) {
		temperatureC = (tempF - 32.0) * 5.0 / 9.0;
		setChanged();
		notifyObservers();
	}

	public void setC(double tempC) {
		temperatureC = tempC;
		setChanged();
		notifyObservers();
	}
}
