package edu.tum.cs.l1.pse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Directory extends FileSystemObject{
	//TODO: add the missing methods
	
	private List<FileSystemObject> children = new ArrayList<FileSystemObject>();
	
	public void addChild(FileSystemObject child) {
		children.add(child);
	}
	
	public void removeChild(FileSystemObject child) {
		children.remove(child);
	}
	
	public List<FileSystemObject> getChildren() {
		return children;
	}
	
	public Directory(String name) {
		super(name);
	}
	

	@Override
	public void list(int level) {
		printName(level);

		Iterator<FileSystemObject> iterator = children.iterator();
		while (iterator.hasNext()) {
			FileSystemObject child = (FileSystemObject) iterator.next();
			child.list(level + 1);
		}
	}



}
