package edu.tum.cs.l1.pse;

public class File extends FileSystemObject {

	public File(String name) {
		super(name);
	}

	@Override
	public void list(int level) {
		printName(level);
	}

}
