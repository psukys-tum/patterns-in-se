package de.tum.cs.i1.pse;

public class Switch {
	private LightSwitchCommand flipUpCommand;
	private LightSwitchCommand flipDownCommand;
	
	public Switch(LightSwitchCommand flipUp, LightSwitchCommand flipDown) {
		this.flipUpCommand = flipUp;
		this.flipDownCommand = flipDown;
	}
	
	public void flipUp() {
		flipUpCommand.execute();
	}
	
	public void flipDown() {
		flipDownCommand.execute();
	}
}
