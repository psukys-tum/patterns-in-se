package de.tum.cs.i1.pse;

public abstract class LightSwitchCommand {
	protected Light theLight;
	
	public LightSwitchCommand(Light lamp) {
		this.theLight = lamp;
	}
	
	public void execute() {
		
	}
}
