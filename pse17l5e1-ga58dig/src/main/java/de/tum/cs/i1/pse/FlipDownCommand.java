package de.tum.cs.i1.pse;

public class FlipDownCommand extends LightSwitchCommand {

	public FlipDownCommand(Light lamp) {
		super(lamp);
	}
	
	public void execute() {
		this.theLight.turnOff();
	}
	
}
