package de.tum.cs.i1.pse;

public class FlipUpCommand extends LightSwitchCommand {

	public FlipUpCommand(Light lamp) {
		super(lamp);
	}

	public void execute() {
		this.theLight.turnOn();
	}
}
