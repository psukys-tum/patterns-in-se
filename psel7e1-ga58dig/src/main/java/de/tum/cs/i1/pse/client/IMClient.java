package de.tum.cs.i1.pse.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import de.tum.cs.i1.pse.utils.Location;

public class IMClient {
	
	private ClientGUI userInterface;
	private Socket clientSocket;
	private ClientMessageHandler clientMessageHandler;
	private ClientDispatcherCommunicationHandler clientDispatcherCommunicationHandler = new ClientDispatcherCommunicationHandler();

	
	public static void main(String[] args) {
		IMClient imClient = new IMClient();
		imClient.startGUI();
	}
	
	
	public void startGUI() {
		userInterface = new ClientGUI();
		userInterface.initGUI();
		userInterface.setVisible(true);
		
		userInterface.setSendButtonActionListener(new OnSend());
		userInterface.setConnectButtonActionListener(new OnLogin());
		userInterface.setDisconnectButtonActionListener(new OnDisconnect());
	}

	public void displayMessage(String message) {
		userInterface.printMessage(message);

	}
	
	public void setGuiFrameName(String name){
		userInterface.setFrameTitle(name);
	}
	
	public void connect(String serverName) {
		try {
			ClientDispatcherCommunicationHandler cdch = new ClientDispatcherCommunicationHandler();
			Location serverLocation = cdch.getServerLocation(serverName);
			
			if (serverLocation != null) {
				if (!serverLocation.getIpAddress().equalsIgnoreCase(
						"unknown host")) {
					System.out
							.println("CLIENT_ELEMENT: Server not found. Shutting down!");
					System.out.println("Ip address: "
							+ serverLocation.getIpAddress() + "Port number: "
							+ serverLocation.getPortNumber());
					clientSocket = new Socket(serverLocation.getIpAddress(),
							serverLocation.getPortNumber());
					System.out
							.println("CLIENT_ELEMENT: Socket created successfuly");
					clientMessageHandler = new ClientMessageHandler(
							clientSocket, this);
					System.out.println("CLIENT_ELEMENT: Client thread created");
					new Thread(clientMessageHandler).start();
					System.out.println("CLIENT_ELEMENT: Client thread started");
				}
			}
		} catch (UnknownHostException e) {
			System.out.println("CLIENT_ELEMENT: Unknown host: localhost");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("CLIENT_ELEMENT: Problem connecting to socket");
			e.printStackTrace();
		}

	}
	
	public void disconnect(){
		clientMessageHandler.dropConnection();
	}
	
	
	private class OnSend implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			clientMessageHandler.sendMessage("Client> "  + userInterface.getClientMessage());
			userInterface.printMessage("Client> "  +userInterface.getClientMessage());
			
			userInterface.blankClientMessageArea();
		}
	}
	
	private class OnLogin implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			connect(userInterface.getServerName());
			userInterface.setConnectButtonVisable(false);
		}	
	}
	
	private class OnDisconnect implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			disconnect();
			userInterface.setConnectButtonVisable(true);
		}
	}
}