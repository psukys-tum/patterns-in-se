package de.tum.cs.i1.pse.dispatcher;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;

import de.tum.cs.i1.pse.utils.Location;


public class Dispatcher {
	
	private ServerSocket dispetcherSocket;
	private DispatcherGUI logScreen;
	private HashMap<String, Location> locationMap = new HashMap<>();

	
	public static void main(String[] args) {	
		Dispatcher dispatcher = new Dispatcher();
		dispatcher.startGUI();
		dispatcher.startListening();
	}


	public void startListening() {
		try {
			dispetcherSocket = new ServerSocket(50001);
			while(true){
				DispatcherMessageHandler thread = new DispatcherMessageHandler(this, dispetcherSocket.accept());
				System.out.println("DISPATCHER: Connection accepted");
				new Thread(thread).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void startGUI() {
		logScreen = new DispatcherGUI();
		logScreen.initGUI();
		logScreen.setVisible();
		logScreen.displayMessage("Starting...ready!");
	}

	public void registerServer(String name, Location location) {
		logRegisterServer(name, location);
		locationMap.put(name, location);
	}
	
	public Location getServerLocation(String name) {
		Location loc = locationMap.get(name);
		logGetServerLocation(name, loc);
		return loc;
	}
	

	private void logRegisterServer(String name, Location location) {
		if(logScreen != null) {
			logScreen.displayMessage("Server '" + name + "' registered.");
			logScreen.displayMessage("IP address: " + location.getIpAddress());
			logScreen.displayMessage("Port number: " + location.getPortNumber());	
		}
	}

	private void logGetServerLocation(String name, Location location) {
		if(logScreen != null) {
			logScreen.displayMessage("Querying for: " + name);
			if(location == null) 
				logScreen.displayMessage("Server not found.");
			else 
				logScreen.displayMessage("Query successful.");
		}
	}
}
