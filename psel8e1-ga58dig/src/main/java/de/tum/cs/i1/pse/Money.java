package de.tum.cs.i1.pse;

public class Money {
	private int amount;
	private String currency;
	private Double depositRate;
	private final Double chf2eu = 0.867861;

	public Money(int amount, String currency, double depositRate) {
		this.amount = amount;
		this.currency = currency;
		this.depositRate = depositRate;
	}
	
	public Money(int amount, String currency) {
		this(amount, currency, 0.0075);
	}
	
	public int getAmount() {
		return amount;
	}

	public String getCurrency() {
		return currency;
	}
	
	public Money add(Money money) {
		Double finalAmount = (double)amount;
		if (money.getCurrency().equals(currency)) {
			finalAmount += money.getAmount();
		} else {
			switch (currency) {
			case "CHF":
				// EU to CHF
				finalAmount += money.getAmount() / chf2eu;
				break;
			case "EU":
				// CHF to EU
				finalAmount += money.getAmount() * chf2eu;
				break;
			}
		}
		return new Money(finalAmount.intValue(), currency);
	}

	public boolean equals(Object object) {
		if (object instanceof Money) {
			Money money = (Money) object;
			return money.getCurrency().equals(currency) && money.getAmount() == amount;
		}
		return false;
	}
	
	public double[] getNextBalances(int nrOfTerms) {
		if (nrOfTerms <= 0) return null;
	
		double[] nextBalances = new double[nrOfTerms];
		
		for (int i=0; i<nrOfTerms; i++) {
			nextBalances[i] = Math.pow(1 + depositRate, i+1)*amount;
		}
		
		return nextBalances;
	}
}