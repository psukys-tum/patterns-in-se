package de.tum.cs.i1.pse;
 
import static org.junit.Assert.*;
 
import org.junit.Test;
 
public class MoneyTest {
    @Test
    public void simpleAdd() {
        Money m12CHF = new Money(12, "CHF");
        Money m14CHF = new Money(14, "CHF");
        Money expected = new Money(26, "CHF");
        Money observed = m12CHF.add(m14CHF);
        assertTrue(expected.equals(observed));
    }
     
     
    @Test
    public void testAddWithDifferentCurrencies(){
    	Money m12CHF = new Money(12, "CHF");
    	Money m14EU  = new Money(14, "EU");
    	Double expectedAmount = 12 + 14 / 0.86;
    	Money expected = new Money(expectedAmount.intValue(), "CHF");
    	Money observed = m12CHF.add(m14EU);
    	assertTrue(expected.equals(observed));
    }
    
    
    @Test
	public void testNextBalances() {
	    	Money m12EUR = new Money(10, "EUR", 0.1);
	    	double[] observed = m12EUR.getNextBalances(2);
	    	
	    // Expected future balances: 11 = 10*(1+0.1) ; 12.1 = 11*(1+0.1)
	    	double[] expected = new double[] {11, 12.1};
	    	assertArrayEquals(expected, observed, 0.1);
    }
     
}