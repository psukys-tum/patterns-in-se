package de.tum.cs.i1.pse;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import de.tum.cs.i1.pse.chat.applicationLayer.ApplicationLayerInterface;
import de.tum.cs.i1.pse.chat.client.ChatClient;
import de.tum.cs.i1.pse.chat.networkLayer.NetworkLayerInterface;
import de.tum.cs.i1.pse.chat.presentationLayer.CaesarEncryption;
import de.tum.cs.i1.pse.chat.presentationLayer.PresentationLayerInterface;
import de.tum.cs.i1.pse.mock.MockNetworkLayer;

public class FunctionalTest {
	
	private static final String SERVER_HOST = "localhost";
	private static final int SERVER_PORT = 1337;

	@Test(timeout = 100)
	public void testIfLayerConfigurationIsCorrect() throws IOException {
		String errorMessage = "Configuration is wrong.";
		
		ChatClient chatClient = new ChatClient(SERVER_HOST, SERVER_PORT);
		ApplicationLayerInterface applicationLayer = chatClient.getApplicationLayer();
		
		assertTrue(errorMessage, applicationLayer instanceof ApplicationLayerInterface);
		assertTrue(errorMessage, applicationLayer.getApplicationLayerListeners().size() >= 1);
		assertTrue(errorMessage, applicationLayer.getPresentationLayer() instanceof PresentationLayerInterface);
		
		PresentationLayerInterface presentationLayer = applicationLayer.getPresentationLayer();
		assertTrue(errorMessage, presentationLayer.getNetworkLayer() instanceof NetworkLayerInterface);
		assertTrue(errorMessage, presentationLayer.getApplicationLayer() instanceof ApplicationLayerInterface);
		
		NetworkLayerInterface networkLayer = presentationLayer.getNetworkLayer();
		assertTrue(errorMessage, networkLayer.getPresentationLayer() instanceof PresentationLayerInterface);
	}
	
	@Test(timeout = 15000)
	public void integrationTest() throws IOException, InterruptedException {
		new Thread(new Runnable() {
			@Override
			public void run() {
				ChatServer chatServer = new ChatServer();
				chatServer.start();
			}
		}).start();
		
		ChatClient chatClient1 = new ChatClient(SERVER_HOST, SERVER_PORT);
		chatClient1.start();

		ChatClient chatClient2 = new ChatClient(SERVER_HOST, SERVER_PORT);
		chatClient2.start();
		
		String testMessage = "test message";
		chatClient1.sendMessage(testMessage);
		Thread.sleep(1000);
		assertTrue(chatClient2.getLastMessageReceived().contains(testMessage));
	}
	
	@Test
	public void testStartOfCaesarEncryptionCallsOpenConnectionOnNetworkLayer() {
		CaesarEncryption caesarEncryption = new CaesarEncryption(12);
		MockNetworkLayer mockNetworkLayer = new MockNetworkLayer();
		caesarEncryption.setNetworkLayer(mockNetworkLayer);
		
		caesarEncryption.start();
		assertTrue(mockNetworkLayer.isOpenConnectionCalled());
	}

}