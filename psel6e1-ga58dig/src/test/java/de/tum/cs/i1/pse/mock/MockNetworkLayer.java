package de.tum.cs.i1.pse.mock;

import de.tum.cs.i1.pse.chat.networkLayer.NetworkLayerInterface;
import de.tum.cs.i1.pse.chat.presentationLayer.PresentationLayerInterface;

public class MockNetworkLayer implements NetworkLayerInterface {

	private boolean openConnectionCalled;

	@Override
	public void sendMessage(String message) {
		

	}

	@Override
	public void receiveMessage(String message) {
	
	}

	@Override
	public void openConnection() {
		this.openConnectionCalled = true;

	}
	
	public boolean isOpenConnectionCalled() {
		return this.openConnectionCalled;
	}

	

	@Override
	public void closeConnection() {
		

	}

	@Override
	public void setPresentationLayer(PresentationLayerInterface presentationLayer) {
		

	}

	@Override
	public PresentationLayerInterface getPresentationLayer() {
		
		return null;
	}

}
