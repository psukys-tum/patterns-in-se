package de.tum.cs.i1.pse.chat.presentationLayer;

import de.tum.cs.i1.pse.chat.applicationLayer.ApplicationLayerInterface;
import de.tum.cs.i1.pse.chat.networkLayer.NetworkLayerInterface;

public interface PresentationLayerInterface {
	public void sendMessage(String message);
	
	public void receiveMessage(String message);
	
	public void start();
	
	public void stop();
	
	public void setNetworkLayer(NetworkLayerInterface layer);
	
	public NetworkLayerInterface getNetworkLayer();
	
	public void setApplicationLayer(ApplicationLayerInterface layer);
	
	public ApplicationLayerInterface getApplicationLayer();
}
