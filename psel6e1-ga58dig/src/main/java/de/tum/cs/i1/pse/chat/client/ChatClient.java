package de.tum.cs.i1.pse.chat.client;

import java.io.IOException;
import java.util.Scanner;

import de.tum.cs.i1.pse.chat.applicationLayer.ApplicationLayer;
import de.tum.cs.i1.pse.chat.applicationLayer.ApplicationLayerInterface;
import de.tum.cs.i1.pse.chat.applicationLayer.ApplicationLayerListener;
import de.tum.cs.i1.pse.chat.networkLayer.NetworkLayerInterface;
import de.tum.cs.i1.pse.chat.networkLayer.TCPNetworkLayer;
import de.tum.cs.i1.pse.chat.presentationLayer.AESEncryption;
import de.tum.cs.i1.pse.chat.presentationLayer.CaesarEncryption;
// In the onMessageReceived method, call the printMessage method and set the lastMessageReceived to the message you received
import de.tum.cs.i1.pse.chat.presentationLayer.PresentationLayerInterface;
public class ChatClient implements ApplicationLayerListener{

	private static final String SERVER_HOST = "ec2-54-201-174-242.us-west-2.compute.amazonaws.com";
	private static final int SERVER_PORT = 1337;
	
	private static final String LOGOUT_MESSAGE = ".logout";
	private static final int CAESAR_KEY = 3;
	private static Thread inputThread;
	private ApplicationLayerInterface applicationLayer;
	private NetworkLayerInterface networkLayer;
	private PresentationLayerInterface presentationLayer;
	
	// For testing only
	private String lastMessageReceived = null;

	public static void main(String[] args) throws IOException {
		ChatClient chatClient = new ChatClient(SERVER_HOST, SERVER_PORT);
		chatClient.start();
		
		inputThread = new Thread(new Runnable() {
			@Override
			public void run() {
				chatClient.waitForUserInput();
			}
		});
		inputThread.start();
		
		System.out.println("ChatClient started.");
	}
	
	public ChatClient(String serverHost, int serverPort) throws IOException {
		applicationLayer = new ApplicationLayer();
		applicationLayer.addApplicationLayerListener(this);
		
		networkLayer = new TCPNetworkLayer(serverHost, serverPort);
		
		presentationLayer = new AESEncryption("0123456701234567".getBytes("UTF-8"));
		
		applicationLayer.setPresentationLayer(presentationLayer);
		networkLayer.setPresentationLayer(presentationLayer);
		
		presentationLayer.setApplicationLayer(applicationLayer);
		presentationLayer.setNetworkLayer(networkLayer);
	}
	
	public void start() {
		applicationLayer.start();
	}
	
	private void waitForUserInput() {
		Scanner scanner = new Scanner(System.in);
		try {
			while (true) {
				String message = scanner.nextLine();
				if (message.equals(LOGOUT_MESSAGE)) {
					shutDown();
				} else {
					sendMessage(message);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
	}
	
	public void sendMessage(String message) {
		applicationLayer.sendMessage(message);
	}

	private void shutDown() {
		applicationLayer.stop();
		inputThread.interrupt();
		System.exit(0);
	}

	private void printMessage(String message) {
		System.out.println(message);
	}
	
	/**
	 * Added for tests
	 * @return The application layer
	 */
	public ApplicationLayerInterface getApplicationLayer() {
		return applicationLayer;
	}

	/**
	 * Added for tests
	 * @return The last message that was received
	 */
	public String getLastMessageReceived() {
		return lastMessageReceived;
	}

	@Override
	public void onMessageReceived(String message) {
		this.lastMessageReceived = message;
		printMessage(message);
	}

}
