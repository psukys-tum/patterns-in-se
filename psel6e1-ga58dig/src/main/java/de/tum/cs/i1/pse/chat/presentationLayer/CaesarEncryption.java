package de.tum.cs.i1.pse.chat.presentationLayer;

import de.tum.cs.i1.pse.chat.applicationLayer.ApplicationLayerInterface;
import de.tum.cs.i1.pse.chat.networkLayer.NetworkLayerInterface;

public class CaesarEncryption implements PresentationLayerInterface {
	
	private NetworkLayerInterface networkLayer;
	private ApplicationLayerInterface applicationLayer;
	
	private final int key;

	public CaesarEncryption(int key) {
		if (key <= 0 || key >= 26) {
			throw new IllegalArgumentException("The key must have a value between 1 to 25");
		}
		this.key = key;
	}

	public String encrypt(String value) {
		String result = new String();
		for (int i = 0; i < value.length(); i++) {
			char ch = value.charAt(i);
			if (ch >= 'A' && ch <= 'Z') {
				ch = shift('A', ch, key);
			} else if (ch >= 'a' && ch <= 'z') {
				ch = shift('a', ch, key);
			}
			result = result + ch;
		}
		return result;
	}

	private char shift(char offset, char input, int key) {
		return (char) (offset + (input - offset + key) % 26);
	}

	public String decrypt(String value) {
		if (value.startsWith("Welcome to chat! Your client ID is")) {
			return value;
		}
		
		if (value.replaceFirst("Client [0-9]+ logged out", "").equals(".")) {
			return value;
		}
		
		String serverPrefix = value.substring(0, value.indexOf(":") + 2);
		value = value.replaceAll("Client [0-9]+: ", "");
		
		String result = new String();
		for (int i = 0; i < value.length(); i++) {
			char ch = value.charAt(i);
			if (ch >= 'A' && ch <= 'Z') {
				ch = shift('A', ch, 26 - key);
			} else if (ch >= 'a' && ch <= 'z') {
				ch = shift('a', ch, 26 - key);
			}
			result = result + ch;
		}
		return serverPrefix + result;
	}
	
	public void receiveMessage(String message) {
		applicationLayer.receiveMessage(decrypt(message));
	}

	public void start() {
		networkLayer.openConnection();
	}

	
	public void stop() {
		networkLayer.closeConnection();
	}
	
	public void setNetworkLayer(NetworkLayerInterface layer) {
		this.networkLayer = layer;
	}
	
	public void setApplicationLayer(ApplicationLayerInterface layer) {
		this.applicationLayer = layer;
	}

	@Override
	public void sendMessage(String message) {
		networkLayer.sendMessage(encrypt(message));
	}

	@Override
	public NetworkLayerInterface getNetworkLayer() {
		return networkLayer;
	}

	@Override
	public ApplicationLayerInterface getApplicationLayer() {
		return applicationLayer;
	}
}
