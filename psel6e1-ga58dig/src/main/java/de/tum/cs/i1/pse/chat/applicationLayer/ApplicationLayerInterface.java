package de.tum.cs.i1.pse.chat.applicationLayer;

import java.util.List;

import de.tum.cs.i1.pse.chat.networkLayer.NetworkLayerInterface;
import de.tum.cs.i1.pse.chat.presentationLayer.PresentationLayerInterface;

public interface ApplicationLayerInterface {	
	void start();
	void stop(); 
	
	void sendMessage(String message);
	void receiveMessage(String message);

	void setPresentationLayer(PresentationLayerInterface presentationLayer);
	PresentationLayerInterface getPresentationLayer();

	void addApplicationLayerListener(ApplicationLayerListener listener);
	void removeApplicationLayerListener(ApplicationLayerListener listener);
	List<ApplicationLayerListener> getApplicationLayerListeners();
	void notifyApplicationLayerListeners(String message);
}
