package de.tum.cs.i1.pse.chat.networkLayer;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

import de.tum.cs.i1.pse.chat.applicationLayer.ApplicationLayerInterface;
import de.tum.cs.i1.pse.chat.presentationLayer.PresentationLayerInterface;

public class TCPNetworkLayer implements NetworkLayerInterface {

	private Socket socket;
	private Scanner socketInput;
	private PrintWriter socketOutput;
	private Thread incomingMessagesThread;

	private String host;
	private int port;
	 
	private PresentationLayerInterface presentationLayer;
	
	public TCPNetworkLayer(String host, int port) throws IOException {
		this.host = host;
		this.port = port;
	}

	@Override
	public void sendMessage(String message) {
		socketOutput.println(message);
	}

	@Override
	public void receiveMessage(String message) {
		this.presentationLayer.receiveMessage(message);
	}
	
	@Override
	public void setPresentationLayer(PresentationLayerInterface presentationLayer) {
		this.presentationLayer = presentationLayer;
	}
	
	@Override
	public PresentationLayerInterface getPresentationLayer() {
		return presentationLayer;
	}
	
	private void waitForIncomingMessages() throws IOException {
		if (incomingMessagesThread != null) {
			return; 
		}

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (socketInput.hasNextLine()) {
					String message = socketInput.nextLine();
					receiveMessage(message);
				}
			}
		}).start();
	}

	@Override
	public void openConnection() {
		System.out.println("Connecting to server ...");
		
		try {
			socket = new Socket(host, port);
			socketInput = new Scanner(new InputStreamReader(socket.getInputStream()));
			socketOutput = new PrintWriter(socket.getOutputStream(), true);
			waitForIncomingMessages();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Connection Stablished.");
	}

	@Override
	public void closeConnection() {
		try {
			if (socketInput != null) {
				socketInput.close();
			}
			
			if (socketOutput != null) {
				socketOutput.close();
			}
			
			if (socket != null) {
				socket.close();
			}
		} catch (IOException e) {
			System.out.print(e.getMessage());
		}
	}
}