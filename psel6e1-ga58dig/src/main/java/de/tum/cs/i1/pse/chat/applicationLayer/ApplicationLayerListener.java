package de.tum.cs.i1.pse.chat.applicationLayer;

public interface ApplicationLayerListener {
	
	void onMessageReceived(String message);

}