package de.tum.cs.i1.pse.chat.presentationLayer;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import de.tum.cs.i1.pse.chat.applicationLayer.ApplicationLayerInterface;
import de.tum.cs.i1.pse.chat.networkLayer.NetworkLayerInterface;



/**
 * Code borrowed from StackOverflow.
 * http://stackoverflow.com/questions/15554296/simple-java-aes-encrypt-decrypt-example
 *
 */
public class AESEncryption implements PresentationLayerInterface {
	
	private byte[] key;
	private String initVector = "RandomInitVector";
	
	private ApplicationLayerInterface applicationLayer;
	private NetworkLayerInterface networkLayer;

	public AESEncryption(byte[] key) {
		this.key = key;
	}

	private String encrypt(String value) {
		try {
			IvParameterSpec parameterSpec = new IvParameterSpec(initVector.getBytes("UTF-8"));
	        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

	        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, parameterSpec);

	        byte[] encrypted = cipher.doFinal(value.getBytes());
	        return Base64.getEncoder().encodeToString(encrypted);
	    } catch (Exception ex) {
	    		ex.printStackTrace();
	    }
		
		return "Errors in encryption";
	}

	private String decrypt(String value) {
		if (value.startsWith("Welcome to chat! Your client ID is")) {
			return value;
		}
		
		if (value.replaceFirst("Client [0-9]+ logged out", "").equals(".")) {
			return value;
		}
		
		String serverPrefix = value.substring(0, value.indexOf(":") + 2);
		value = value.replaceAll("Client [0-9]+: ", ""); 
		
		try {
			IvParameterSpec parameterSpec = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

	        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, parameterSpec);
	            
	        byte[] original = cipher.doFinal(Base64.getDecoder().decode(value));
	        return serverPrefix + new String(original);
	    } catch (Exception ex) {
	    		ex.printStackTrace();
	    }
		
        return "Errors in decryption";
	}
	
	public void sendMessage(String message) {
		networkLayer.sendMessage(encrypt(message));
	}
	
	public void receiveMessage(String message) {
		applicationLayer.receiveMessage(decrypt(message));
	}

	public void start() { 
		networkLayer.openConnection();
	}
	
	public void stop() {
		networkLayer.closeConnection();
	}
	
	public void setNetworkLayer(NetworkLayerInterface layer) {
		this.networkLayer = layer;
	}
	
	public void setApplicationLayer(ApplicationLayerInterface layer) {
		this.applicationLayer = layer;
	}

	@Override
	public NetworkLayerInterface getNetworkLayer() {
		return networkLayer;
	}

	@Override
	public ApplicationLayerInterface getApplicationLayer() {
		return applicationLayer;
	}
}
