package de.tum.cs.i1.pse.chat.applicationLayer;

import java.util.ArrayList;
import java.util.List;

import de.tum.cs.i1.pse.chat.networkLayer.NetworkLayerInterface;
import de.tum.cs.i1.pse.chat.presentationLayer.PresentationLayerInterface;

public class ApplicationLayer implements ApplicationLayerInterface {

	private PresentationLayerInterface presentationLayer;
	private List<ApplicationLayerListener> applicationLayerListeners = new ArrayList<>();
	
	
	@Override
	public void start() {
		presentationLayer.start();

	}

	@Override
	public void stop() {
		presentationLayer.stop();
	}

	@Override
	public void sendMessage(String message) {
		presentationLayer.sendMessage(message);

	}

	@Override
	public void receiveMessage(String message) {
		notifyApplicationLayerListeners(message);

	}

	@Override
	public void addApplicationLayerListener(ApplicationLayerListener listener) {
		this.applicationLayerListeners.add(listener);
	}

	@Override
	public void removeApplicationLayerListener(ApplicationLayerListener listener) {
		this.applicationLayerListeners.remove(this.applicationLayerListeners.indexOf(listener));
	}

	@Override
	public List<ApplicationLayerListener> getApplicationLayerListeners() {
		return this.applicationLayerListeners;
	}

	@Override
	public void notifyApplicationLayerListeners(String message) {
		for (ApplicationLayerListener listener: this.applicationLayerListeners) {
			listener.onMessageReceived(message);
		}

	}
	
	public void setPresentationLayer(PresentationLayerInterface presentationLayer) {
		this.presentationLayer = presentationLayer;
	}

	@Override
	public PresentationLayerInterface getPresentationLayer() {
		return presentationLayer;
	}

}
