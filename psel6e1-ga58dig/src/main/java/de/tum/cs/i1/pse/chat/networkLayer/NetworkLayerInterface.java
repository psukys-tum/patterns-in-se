package de.tum.cs.i1.pse.chat.networkLayer;

import de.tum.cs.i1.pse.chat.presentationLayer.PresentationLayerInterface;

public interface NetworkLayerInterface {
	
	void openConnection();
	void closeConnection();

	void sendMessage(String message);
	void receiveMessage(String message);
	
	void setPresentationLayer(PresentationLayerInterface presentationLayer);
	PresentationLayerInterface getPresentationLayer();
}
