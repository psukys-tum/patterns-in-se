package de.tum.cs.i1.pse.factory.toppingfactory;

import de.tum.cs.i1.pse.factory.topings.pepperoni.Pepperoni;
import de.tum.cs.i1.pse.factory.topings.sauce.Sauce;
import de.tum.cs.i1.pse.factory.topings.veggies.Veggies;
import de.tum.cs.i1.pse.factory.toppings.cheese.Cheese;
import de.tum.cs.i1.pse.factory.toppings.clams.Clams;
import de.tum.cs.i1.pse.factory.toppings.dough.Dough;
import de.tum.cs.i1.pse.factory.toppings.meat.Meat;

public class BavarianPizzaToppingFactory implements PizzaToppingFactory {

	@Override
	public Dough createDough() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Sauce createSauce() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cheese createCheese() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Veggies[] createVeggies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Pepperoni createPepperoni() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Clams createClam() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Meat createMeat() {
		// TODO Auto-generated method stub
		return null;
	}

}
