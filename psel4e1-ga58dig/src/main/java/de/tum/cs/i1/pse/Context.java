package de.tum.cs.i1.pse;

public class Context {
	private SortStrategy sortAlgorithm;
	private int[] array;
	
	public void setArray(int[] arr) {
		this.array = arr;
	}
	
	public void setSortAlgorithm(SortStrategy algo) {
		this.sortAlgorithm = algo;
	}
	
	public SortStrategy getSortAlgorithm() {
		return this.sortAlgorithm;
	}
	
	public void sort() {
		this.sortAlgorithm.performSort(this.array);
	}
}
