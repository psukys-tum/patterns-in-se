package de.tum.cs.i1.pse;

public class Policy {

	Context context;
	
	public Policy(Context context) {
		// set the context
		this.context = context;
	}

	public void configure(boolean timeIsImportant, boolean spaceIsImportant) {
		if (timeIsImportant && spaceIsImportant) {
			this.context.setSortAlgorithm(new QuickSort());
		} else {
			this.context.setSortAlgorithm(new MergeSort());
		}
	}
}
