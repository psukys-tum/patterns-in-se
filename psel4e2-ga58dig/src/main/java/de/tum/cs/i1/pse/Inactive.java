package de.tum.cs.i1.pse;

public class Inactive implements State{

	protected int computeVampirePower() {
		return 42;
	}

	private void LEDOff() {
		System.out.println("LED turned Off.");
	}

	@Override
	public void entry() {
	}

	@Override
	public void exit() {
	}

	@Override
	public void handle(MusicPlayer player, Event event) {
	}
}
