package de.tum.cs.i1.pse;

public class On implements State{

	protected int computeVampirePower() {
		return 42;
	}

	private void LEDOn() {
		System.out.println("LED turned On.");
	}

	@Override
	public void entry() {
	}

	@Override
	public void exit() {
	}

	@Override
	public void handle(MusicPlayer player, Event event) {
	}

}
