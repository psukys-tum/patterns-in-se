package de.tum.cs.i1.pse;

public class PlayingMusic implements State{

	protected int computeVampirePower() {
		return 42;
	}

	private void playMusic() {
		System.out.println("Play Music");
	}

	@Override
	public void entry() {
	}

	@Override
	public void exit() {
	}

	@Override
	public void handle(MusicPlayer player, Event event) {
	}
}
