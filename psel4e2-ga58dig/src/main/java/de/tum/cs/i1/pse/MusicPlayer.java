package de.tum.cs.i1.pse;

public class MusicPlayer {

	private State currentState;

	public MusicPlayer() {
		this.currentState = new Inactive();
	}

	public void setCurrentState(State currentState) {
		if (this.currentState != null) {
			this.currentState.exit();
		}
		this.currentState = currentState;
		this.currentState.entry();
	}

	public State getCurrentState() {
		return currentState;
	}

	public void handle(Event event) {
		switch(event) {
			case OnOffPressed:
				if (currentState instanceof Inactive) {
					turnOn();
				} else if (currentState instanceof On ||
						   currentState instanceof Paused) {
					turnOff();
				}
				break;
			case PlayPausePressed:
				if (currentState instanceof PlayingMusic) {
					pauseMusic();
				} else if (currentState instanceof On ||
						   currentState instanceof Paused){
					playMusic();
				}
				break;
			case SleepTimeExceeded:
				break;
		}
	}

	public void playMusic() {
		setCurrentState(new PlayingMusic());
		System.out.println("Playing Music");
	}

	public void pauseMusic() {
		setCurrentState(new Paused());
		System.out.println("Pausing the Music");
	}

	public void turnOn() {
		setCurrentState(new On());
		System.out.println("Music Player turned On");
	}

	public void turnOff() {
		setCurrentState(new Inactive());
		System.out.println("Music Player turned Off");
	}
}
