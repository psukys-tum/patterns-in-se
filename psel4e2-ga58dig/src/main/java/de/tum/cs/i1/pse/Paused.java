package de.tum.cs.i1.pse;

public class Paused implements State{

	protected int computeVampirePower() {
		return 42;
	}

	private void pauseMusic() {
		System.out.println("Pause Music");
	}

	private void checkTimeout() {
		System.out.println("Check Timeout");
	}

	@Override
	public void entry() {
	}

	@Override
	public void exit() {
	}

	@Override
	public void handle(MusicPlayer player, Event event) {
	}

}
