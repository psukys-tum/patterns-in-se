package edu.tum.cs.i1.pse;

public class EnterpriseEncryption extends Encryption{
	
	public EnterpriseEncryption() {
		imp = new Caesar();
	}
	
	public String encrypt(String m, byte key) {
		if (key < 10)
			throw new RuntimeException("Key cannot be less than 10");
		
		StringBuilder sb = new StringBuilder();
		for(String w: m.split(" ")) {
			sb.append(imp.encryptWord(w, key));
			sb.append(" ");
		}
		
		return sb.toString();
	}
	
	public String decrypt(String c, byte key) {
		if (key < 10)
			throw new RuntimeException("Key cannot be less than 10");
		
		StringBuilder sb = new StringBuilder();
		for(String w: c.split(" ")) {
			sb.append(imp.decryptWord(w, key));
			sb.append(" ");
		}
		
		return sb.toString();
	}
}
