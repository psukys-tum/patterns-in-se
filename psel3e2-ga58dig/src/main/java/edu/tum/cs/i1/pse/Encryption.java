package edu.tum.cs.i1.pse;

//import java.util.Arrays;
//import java.util.stream.Collectors;

public abstract class Encryption {
	Cipher imp;
	
	public abstract String encrypt(String m, byte key);
	
	public abstract String decrypt(String c, byte key);
}
