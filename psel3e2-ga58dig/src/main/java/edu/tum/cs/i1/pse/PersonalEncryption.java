package edu.tum.cs.i1.pse;

public class PersonalEncryption extends Encryption{
	
	public PersonalEncryption() {
		imp = new Transpose();
	}
	
	public String encrypt(String m, byte key) {
		StringBuilder sb = new StringBuilder();
		for(String w: m.split(" ")) {
			sb.append(imp.encryptWord(w, key));
			sb.append(" ");
		}
		
		return sb.toString();
	}
	
	public String decrypt(String c, byte key) {
		StringBuilder sb = new StringBuilder();
		for(String w: c.split(" ")) {
			sb.append(imp.decryptWord(w, key));
			sb.append(" ");
		}
		
		return sb.toString();
	}
}
