package edu.tum.cs.i1.pse;

public class CryptoSystem {

	private Encryption impl;

	public CryptoSystem(String encryptionType) {

		if (encryptionType.equalsIgnoreCase("Enterprise"))			
			impl = new EnterpriseEncryption();
		else
			impl = new PersonalEncryption();
	}

	public String encryptDoc(String plain, byte key) {
		return impl.encrypt(plain, key);
	}

	public String decryptDoc(String secret, byte key) {
		return impl.decrypt(secret, key);
	}

}
