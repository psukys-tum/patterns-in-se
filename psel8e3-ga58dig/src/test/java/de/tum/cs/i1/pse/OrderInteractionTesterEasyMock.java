package de.tum.cs.i1.pse;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.easymock.EasyMockRunner;
import org.easymock.TestSubject;
import org.easymock.Mock;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.tum.cs.i1.pse.Order;
import de.tum.cs.i1.pse.OrderImpl;
import de.tum.cs.i1.pse.Warehouse;

@RunWith(EasyMockRunner.class)
public class OrderInteractionTesterEasyMock {
	
	private static String TALISKER = "Talisker";

	@TestSubject
    private Order order = new OrderImpl(TALISKER, 50);
	
	@Mock
	private Warehouse warehouseMock;
	
	@Test
	public void fillingRemovesInventoryIfInStock() {
		//Specify behavior of the collaborator
		expect(warehouseMock.hasInventory(TALISKER, 50)).andReturn(true);
		warehouseMock.remove(TALISKER, 50);
		//Get ready for Replay
		replay(warehouseMock);
		//Execute SUT
		order.fillOut(warehouseMock);
		assertTrue(order.isFilled());
		//Verify that the observed behavior is equal to the specified behavior
		verify(warehouseMock);
	}	
	
	@Test
	public void fillingDoesNotRemoveifNotEnoughInStock() {
		expect(warehouseMock.hasInventory(TALISKER, 51)).andReturn(false);
		
		warehouseMock.remove(TALISKER, 51);
		replay(warehouseMock);
		order.fillOut(warehouseMock);
		assertFalse(order.isFilled());
		
		verify(warehouseMock);
	}

}
