package edu.tum.cs.l2.pse;

public class SUVCar extends Car {

	protected boolean terrainMode;

	public boolean hasTerrainMode() {
		return terrainMode;
	}
	
	public void setTerrainMode(boolean val) {
		terrainMode = val;
	}
	
	public int getDailyCost() {
		return terrainMode? 14 : 12;
	}
}
