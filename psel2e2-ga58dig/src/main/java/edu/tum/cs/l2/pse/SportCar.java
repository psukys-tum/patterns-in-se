package edu.tum.cs.l2.pse;

public class SportCar extends Car {
	protected boolean superCharger;
	
	public boolean hasSuperCharger() {
		return superCharger;
	}
	public void setSuperCharger(boolean val) {
		superCharger = val;
	}

	public int getDailyCost() {
		return superCharger? 12 : 10;
	}

}
