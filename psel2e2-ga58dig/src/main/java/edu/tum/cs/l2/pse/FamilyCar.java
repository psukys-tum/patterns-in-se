package edu.tum.cs.l2.pse;

public class FamilyCar extends Car {

	protected boolean childSeat;

	public void setChildSeat(boolean val) {
		childSeat = val;
	}
	
	public boolean hasChildSeat() {
		return childSeat;
	}

	public int getDailyCost() {
		return childSeat? 9 : 8;
	}
}
