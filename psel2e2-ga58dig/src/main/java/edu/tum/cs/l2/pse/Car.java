package edu.tum.cs.l2.pse;

public class Car {
	protected String color;
	protected int plateNumber;
	protected int rentDailyCost;
	protected int capacity;
	
	public int getDailyCost() {
		return rentDailyCost;
	};
	String getColor() {
		return color;
	};
	void setColor(String color) {
		color = color;
	};
	
	
	int getPlateNumber() {
		return plateNumber;
	}
	void setPlateNumber(int plateNumber) {
		plateNumber = plateNumber;
	}
	
}
