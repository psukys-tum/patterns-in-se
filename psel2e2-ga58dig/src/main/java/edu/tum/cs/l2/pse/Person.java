package edu.tum.cs.l2.pse;

import java.util.Date;

public class Person {

	private String name;
	private Date dateOfBirth;
	private String drivingLicenceNumber;
	
	public Person(String nam) {
		name = nam;
	}
	
	public Person(String nam, Date birthDat, String drivingLic) {
		name = nam;
		dateOfBirth = birthDat;
		drivingLicenceNumber = drivingLic;
	}
}
