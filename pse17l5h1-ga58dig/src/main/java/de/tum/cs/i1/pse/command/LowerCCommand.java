package de.tum.cs.i1.pse.command;

import de.tum.cs.i1.pse.IllegalTemperatureException;
import de.tum.cs.i1.pse.model.TemperatureModel;

public class LowerCCommand extends Command {

	private double oldValue, newValue;
	private TemperatureModel model;
	
	public LowerCCommand(TemperatureModel model) {
		this.model = model;
		oldValue = model.getC();
	}
	
	@Override
	public void execute() throws IllegalTemperatureException {
		newValue = oldValue - 1;
		model.setC(newValue);
	}

	@Override
	public void undo() throws IllegalTemperatureException {
		model.setC(oldValue);
	}

	@Override
	public void redo() throws IllegalTemperatureException {
		model.setC(newValue);
	}

}
