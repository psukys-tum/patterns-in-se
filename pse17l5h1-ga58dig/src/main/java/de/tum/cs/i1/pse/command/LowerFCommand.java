package de.tum.cs.i1.pse.command;

import de.tum.cs.i1.pse.IllegalTemperatureException;
import de.tum.cs.i1.pse.model.TemperatureModel;

public class LowerFCommand extends Command {

	private double oldValue, newValue;
	private TemperatureModel model;
	
	public LowerFCommand(TemperatureModel model) {
		this.model = model;
		oldValue = model.getF();
	}
	
	@Override
	public void execute() throws IllegalTemperatureException {
		newValue = oldValue - 1;
		model.setF(newValue);
	}

	@Override
	public void undo() throws IllegalTemperatureException {
		model.setF(oldValue);
	}

	@Override
	public void redo() throws IllegalTemperatureException {
		model.setF(newValue);
	}

}
