package de.tum.cs.i1.pse.command;

import de.tum.cs.i1.pse.IllegalTemperatureException;
import de.tum.cs.i1.pse.model.TemperatureModel;

public class RaiseKCommand extends Command {

	private double oldValue, newValue;
	private TemperatureModel model;
	
	public RaiseKCommand(TemperatureModel model) {
		this.model = model;
		oldValue = model.getK();
	}
	
	@Override
	public void execute() throws IllegalTemperatureException {
		oldValue = model.getK();
		newValue = oldValue + 1;
		model.setK(newValue);
	}

	@Override
	public void undo() throws IllegalTemperatureException {
		model.setK(oldValue);
	}

	@Override
	public void redo() throws IllegalTemperatureException {
		model.setK(newValue);
	}

}
