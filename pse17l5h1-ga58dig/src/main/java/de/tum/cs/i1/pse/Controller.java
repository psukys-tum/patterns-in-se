package de.tum.cs.i1.pse;

import de.tum.cs.i1.pse.command.LowerCCommand;
import de.tum.cs.i1.pse.command.LowerFCommand;
import de.tum.cs.i1.pse.command.LowerKCommand;
import de.tum.cs.i1.pse.command.RaiseCCommand;
import de.tum.cs.i1.pse.command.RaiseFCommand;
import de.tum.cs.i1.pse.command.RaiseKCommand;
import de.tum.cs.i1.pse.model.TemperatureModel;

public class Controller {
	
	Invoker manageInvoker;
	TemperatureModel model;
	
	public Controller(TemperatureModel model) {
		this.model = model;
		manageInvoker = new Invoker();
	}
	
	public void increaseC() throws IllegalTemperatureException{
		manageInvoker.execute(new RaiseCCommand(model));
	}

	public void decreaseC() throws IllegalTemperatureException {
		manageInvoker.execute(new LowerCCommand(model));
	}
	
	public void increaseK() throws IllegalTemperatureException {
		manageInvoker.execute(new RaiseKCommand(model));
	}
	
	public void decreaseK() throws IllegalTemperatureException {
		manageInvoker.execute(new LowerKCommand(model));
	}
	
	public void increaseF() throws IllegalTemperatureException {
		manageInvoker.execute(new RaiseFCommand(model));
	}
	
	public void decreaseF() throws IllegalTemperatureException {
		manageInvoker.execute(new LowerFCommand(model));
	}
	
	public void undo() throws IllegalTemperatureException{
		if (manageInvoker.isUndoable()) {
			manageInvoker.undo();
		}
	}
	
	public void redo() throws IllegalTemperatureException{
		if (manageInvoker.isRedoable()) {
			manageInvoker.redo();
		}
	}
	
	public void setC(double c) throws IllegalTemperatureException {
		this.model.setC(c);
	}	
	
	public void setK(double k) throws IllegalTemperatureException {
		this.model.setK(k);
	}
	
	public void setF(double f) throws IllegalTemperatureException {
		this.model.setF(f);
	}
	
	public double getC() {
		return this.model.getC();
	}
	
	public double getK() {
		return this.model.getK();
	}
	
	public double getF() {
		return this.model.getF();
	}
}
