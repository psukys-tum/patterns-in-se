package de.tum.cs.i1.pse.driver;

import de.tum.cs.i1.pse.client.dictionary.DictionaryClient;
import de.tum.cs.i1.pse.client.translation.TranslationClient;
import de.tum.cs.i1.pse.middleware.Broker;
import de.tum.cs.i1.pse.middleware.client.Client;
import de.tum.cs.i1.pse.middleware.server.Server;
import de.tum.cs.i1.pse.server.dictionary.DictionaryServer;
import de.tum.cs.i1.pse.server.translation.TranslationServer;

public class Driver {

	public static void main(String[] args) {
		/* Imaginary network node 0 */
		Broker broker = new Broker();
		
		/*----------------------------------------------------------------------------------*/
		/* Imaginary network node 1 */
		
		DictionaryClient dictionaryClient = new DictionaryClient();
		TranslationClient translationClient = new TranslationClient();
		dictionaryClient.setBroker(broker);
		translationClient.setBroker(broker);
		
		/*----------------------------------------------------------------------------------*/
		/* Imaginary network node 2 */
		
		TranslationServer translationServer = new TranslationServer("Translation server");
		translationServer.setBroker(broker);
		
		/*----------------------------------------------------------------------------------*/
		/* Imaginary network node 3 */
		
		DictionaryServer dictionaryServer = new DictionaryServer("Dictionary server");
		dictionaryServer.setBroker(broker);
		
		
		/*----------------------------------------------------------------------------------*/
		
		dictionaryClient.callServer();
		translationClient.callServer();
	}
	
}
