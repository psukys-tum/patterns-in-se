package de.tum.cs.i1.pse;

import static org.junit.Assert.*;

import org.junit.Test;

import de.tum.cs.i1.pse.client.translation.TranslationClient;
import de.tum.cs.i1.pse.middleware.Broker;
import de.tum.cs.i1.pse.middleware.server.Server;
import de.tum.cs.i1.pse.server.translation.TranslationServer;
import de.tum.cs.i1.pse.service.translation.TranslationService;

public class FunctionalTest {
	
	@Test
	public void integrationTest() {
		Broker broker = new Broker();
		TranslationClient translationClient = new TranslationClient();
		translationClient.setBroker(broker);
		
		Server translationServer = new TranslationServer(Broker.SERVER_NAME_TRANSLATION);
		translationServer.setBroker(broker);
		
		String actualValue = translationClient.translateWord("Dog", TranslationService.LANGUAGE_FRENCH);
		assertEquals("Chien", actualValue);
	}

}
