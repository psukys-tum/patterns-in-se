package de.tum.cs.i1.pse.middleware;

import de.tum.cs.i1.pse.middleware.server.Server;

public class RegisteredService {

	private String serviceName;
	private Server server;
	
	public RegisteredService(Server server, String serviceName) {
		this.serviceName = serviceName;
		this.server = server;
	}

	public String getServiceName() {
		return serviceName;
	}
	
	public Server getServer() {
		return server;
	}

	@Override
	public String toString() {
		return "RegisteredService [ServerName=" + server.getName() 
				+ ", ServiceName=" + serviceName + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		RegisteredService other = (RegisteredService) obj;
		
		if (server.getName() == null) {
			if (other.server.getName() != null) {
				return false;
			}
		} else if (!server.getName().equals(other.server.getName())) {
			return false;
		}
		
		if (serviceName == null) {
			if (other.serviceName != null) {
				return false;
			}
		} else if (!serviceName.equals(other.serviceName)) {
			return false;
		}
		
		return true;
	}
	
}
