package de.tum.cs.i1.pse.middleware.client;

import de.tum.cs.i1.pse.middleware.Broker;

public interface Client {

	void setBroker(Broker broker);

	void callServer();

}