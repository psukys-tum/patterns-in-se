package de.tum.cs.i1.pse.middleware.proxy;

import de.tum.cs.i1.pse.middleware.Broker;
import de.tum.cs.i1.pse.service.dictionary.DictionaryService;

public class DictionaryClientSideProxy extends ClientSideProxy implements DictionaryService {

	public DictionaryClientSideProxy(Broker broker) {
		super(broker);
	}

	public String lookupWord(String word) {
		return this.sendRequest(DictionaryService.SERVICE_NAME_DICTIONARY, word);
	}

}
