package de.tum.cs.i1.pse.middleware.proxy;

import de.tum.cs.i1.pse.middleware.Broker;


/**
 * ClientProxy that encapsulate communication with broker and marshaling/unmarshaling
 */
public class ClientSideProxy {
	
	protected Broker broker = null;

	public ClientSideProxy(Broker broker) {
		this.broker = broker;
	}
	
	/**
	 * Forwards the service request from client to broker and does the marshaling/unmarshaling
	 * @param serviceName Service to be initiated
	 * @param parameters Parameters for service (here: String)
	 * @return Result of service execution (here: String)
	 */
	public String sendRequest(String serviceName, String parameters) {
		String packedData = packData(parameters);
		String result = broker.forwardRequest(serviceName, packedData);
		String unpackedData = unpackData(result);
		return unpackedData;
	}
	
	/**
	 * Performs the marshaling
	 * @param inputData 
	 * @return marshaled data
	 */
	public String packData(String inputData) {
		// Marshaling should be done here
		return inputData;
	}
	
	/**
	 * Performs the unmarshaling
	 * @param inputData
	 * @return unmarshaled data
	 */
	public String unpackData(String inputData) {
		// Unmarshaling should be done here
		return inputData;
	}
	
}
