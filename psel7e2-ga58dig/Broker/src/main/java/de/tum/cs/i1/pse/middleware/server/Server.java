package de.tum.cs.i1.pse.middleware.server;

import de.tum.cs.i1.pse.middleware.Broker;

public interface Server {

	String runService(String serviceName, String parameter);

	/**
	 * Set a broker for this server and register all services of this server
	 */
	void setBroker(Broker broker);

	String getName();

	Broker getBroker();

}