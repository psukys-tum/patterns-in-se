package de.tum.cs.i1.pse.middleware.proxy;

import de.tum.cs.i1.pse.middleware.server.Server;

/**
 * Implements marshaling and unmarshaling of the server component
 */
public class ServerSideProxy {
	
	private Server server;
		
	public ServerSideProxy(Server server) {
		this.server = server;
	}

	/**
	 * Performs the marshaling
	 * @param inputData
	 * @return Marshaled data
	 */
	public String packData(String inputData) {
		// Marshaling should be done here
		return inputData;
	}
	
	/**
	 * Performs the unmarshaling
	 * @param inputData
	 * @return Unmarshaled data
	 */
	public String unpackData(String inputData) {
		// Unmarshaling should be done here
		return inputData;
	}
	
	/**
	 * Forward service call to server
	 * @param serviceName Name of service
	 * @param parameters Parameters of service invocation (here: a String)
	 * @return Result of service invocation (here: a String)
	 */
	public String callService(String serviceName, String parameters) {
		String unpackedParameters = unpackData(parameters);
		String result = this.server.runService(serviceName, unpackedParameters);
		String packedResult = packData(result);
		return packedResult;
	}
	
}