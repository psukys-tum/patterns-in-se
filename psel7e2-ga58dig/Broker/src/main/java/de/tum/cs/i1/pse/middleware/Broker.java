package de.tum.cs.i1.pse.middleware;

import java.util.ArrayList;
import java.util.List;

import de.tum.cs.i1.pse.middleware.proxy.DictionaryClientSideProxy;
import de.tum.cs.i1.pse.middleware.proxy.ServerSideProxy;
import de.tum.cs.i1.pse.middleware.proxy.TranslationClientSideProxy;
import de.tum.cs.i1.pse.middleware.server.Server;
import de.tum.cs.i1.pse.service.Service;
import de.tum.cs.i1.pse.service.dictionary.DictionaryService;
import de.tum.cs.i1.pse.service.translation.TranslationService;

public class Broker {

	public static final String SERVER_NAME_TRANSLATION = "Translation server";
	public static final String SERVER_NAME_DICTIONARY = "Dictionary server";
	
	/**
	 * List of all registered services
	 */
	protected List<RegisteredService> services;
	
	public Broker() {
		this.services = new ArrayList<>();
	}
	
	/**
	 * Allows to register a service at this broker
	 * A service is identified by the combination of server name and service name
	 * @param server Instance of service proxy
	 * @param serviceName Name of service
	 * @return Success indicator of registering
	 */
	public boolean registerService(Server server, String serviceName) {
		RegisteredService service = new RegisteredService(server, serviceName);
		if (!services.contains(service)) {
			services.add(service);
			System.out.println("Registered service " + service);
		}
		
		return true;
	}
	
	/**
	 * Unregister a service previously registered
	 * @param server Instance of service proxy
	 * @param serviceName Name of service
	 * @return Success indicator of unregister
	 */
	public boolean unregisterService(Server server, String serviceName) {
		RegisteredService service = new RegisteredService(server, serviceName);
		if (services.contains(service)) {
			services.remove(service);
			System.out.println("Deregistered service " + service);
		}
		
		return true;
	}

	public Service getService(String serviceName) {
		switch (serviceName) {
		case DictionaryService.SERVICE_NAME_DICTIONARY:
			return new DictionaryClientSideProxy(this); 
		case TranslationService.SERVICE_NAME_TRANSLATION_GERMAN:
		case TranslationService.SERVICE_NAME_TRANSLATION_FRENCH:
			return new TranslationClientSideProxy(this); 
		default:
			return null; 
		}
	}

	/**
	 * @param serviceName Name of service
	 * @param parameters Parameters for service invocation
	 * @return Result of service invocation
	 */
	public String forwardRequest(String serviceName, String parameter) {
		ServerSideProxy proxy = findServer(serviceName);
		if (proxy != null) {
			return proxy.callService(serviceName, parameter);
		} else {
			System.out.println("No service found for ServiceName='" + serviceName + "'");
			return null;
		}
	}

	private ServerSideProxy findServer(String serviceName) {
		for (RegisteredService service : this.listServices()) {
			if (service.getServiceName().equals(serviceName)) {
				return new ServerSideProxy(service.getServer());
			}
		}
		return null;
	}

	private List<RegisteredService> listServices() {
		return services;
	}

}