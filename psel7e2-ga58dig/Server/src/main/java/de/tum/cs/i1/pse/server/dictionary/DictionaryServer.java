package de.tum.cs.i1.pse.server.dictionary;

import java.util.ArrayList;

import de.tum.cs.i1.pse.server.AbstractServer;
import de.tum.cs.i1.pse.service.dictionary.DictionaryService;

public class DictionaryServer extends AbstractServer implements DictionaryService {

	public DictionaryServer(String name) {
		serviceNames = new ArrayList<String>();
		this.name = name;
		initializeServices();
	}
	
	/**
	 * Initializes several services of this server
	 */
	@Override
	protected void initializeServices() {
		this.serviceNames.add(DictionaryService.SERVICE_NAME_DICTIONARY);
	}

	/**
	 * Executes a service on this server
	 * @param serviceName: Name of service to execute
	 * @param parameters: Parameters for the service (here: a String)
	 * @return Result of service execution (here: a String)
	 */
	@Override
	public String runService(String serviceName, String parameter) {
		System.out.println("AbstractServer '" + name + "': Running service '" + serviceName + "' with parameters '" + parameter + "'");
		String result = null; 
		
		if (serviceName.equals(DictionaryService.SERVICE_NAME_DICTIONARY)) {
			result = lookupWord(parameter);
		} else {
			System.out.println("Can't offer service " + serviceName);
		}
		
		return result;
	}

	@Override
	public String lookupWord(String word) {
		switch (word) {
		case "Dog":
			return "a domesticated carnivorous mammal that typically has a long snout, an acute sense of smell, non-retractile claws, and a barking, howling, or whining voice.";
		case "Cat": 
			return "a small domesticated carnivorous mammal with soft fur, a short snout, and retractile claws. It is widely kept as a pet or for catching mice, and many breeds have been developed.";
		default:
			return null;
		}
	}

}