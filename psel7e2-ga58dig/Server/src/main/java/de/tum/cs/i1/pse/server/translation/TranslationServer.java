package de.tum.cs.i1.pse.server.translation;

import java.util.ArrayList;

import de.tum.cs.i1.pse.server.AbstractServer;
import de.tum.cs.i1.pse.service.translation.TranslationService;

public class TranslationServer extends AbstractServer implements TranslationService {
	
	public TranslationServer(String name) {
		this.name = name;
		this.serviceNames = new ArrayList<String>();
		initializeServices();
	}
	
	/**
	 * Initializes several services of this server
	 */
	@Override
	protected void initializeServices() {
		this.serviceNames.add(TranslationService.SERVICE_NAME_TRANSLATION_GERMAN);
		this.serviceNames.add(TranslationService.SERVICE_NAME_TRANSLATION_FRENCH);
	}

	/**
	 * Executes a service on this server
	 * @param serviceName: Name of service to execute
	 * @param parameters: Parameters for the service (here: a String)
	 * @return Result of service execution (here: a String)
	 */
	@Override
	public String runService(String serviceName, String parameter) {
		System.out.println("AbstractServer '" + name + "': Running service '" + serviceName + "' with parameters '" + parameter + "'");
		
		String result = null; 
		if (serviceName.equals(TranslationService.SERVICE_NAME_TRANSLATION_GERMAN)) {
			result = translate(parameter, TranslationService.LANGUAGE_GERMAN);
		} else if (serviceName.equals(TranslationService.SERVICE_NAME_TRANSLATION_FRENCH)){
			result = translate(parameter, TranslationService.LANGUAGE_FRENCH);
		} else {
			System.out.println("Can't offer service " + serviceName);
		}
		
		return result;
	}

	public String translate(String word, String language) {
		switch (language) {
		case TranslationService.LANGUAGE_GERMAN:
			return translateToGerman(word); 
		case TranslationService.LANGUAGE_FRENCH:
			return translateToFrench(word);
		default:
			throw new IllegalStateException("Illegal language requested: " + language); 
		}
	}

	private String translateToGerman(String word) {
		switch (word) {
		case "Dog":
			return "Hund";
		case "Cat": 
			return "Katze";
		default:
			System.out.println("AbstractServer '" + name + "': Word not in dictionary: " + word);
			return null; 
		}
	}

	private String translateToFrench(String word) {
		switch (word) {
		case "Dog":
			return "Chien";
		case "Cat": 
			return "Chatte";
		default:
			System.out.println("AbstractServer '" + name + "': Word not in dictionary: " + word);
			return null; 
		}
	}
	
}