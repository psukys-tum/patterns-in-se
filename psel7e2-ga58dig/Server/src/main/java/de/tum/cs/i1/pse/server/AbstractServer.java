package de.tum.cs.i1.pse.server;

import java.util.List;

import de.tum.cs.i1.pse.middleware.Broker;
import de.tum.cs.i1.pse.middleware.server.Server;

public abstract class AbstractServer implements Server {
	
	@Override
	public abstract String runService(String serviceName, String parameter);

	protected abstract void initializeServices();

	private Broker broker;
	protected String name;
	protected List<String> serviceNames;

	public AbstractServer() {
		super();
	}

	@Override
	public void setBroker(Broker newBroker) {
		// Unregister services on old broker
		if (this.broker != null) {
			for (String service : serviceNames) {
				this.broker.unregisterService(this, service);
			}
		}
				
		this.broker = newBroker;
		
		// Register all services on new broker
		for (String service : serviceNames) {
			this.broker.registerService(this, service);
		}
		
		System.out.println();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public Broker getBroker() {
		return this.broker;
	}

}