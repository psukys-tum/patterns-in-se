package de.tum.cs.i1.pse.client;

import de.tum.cs.i1.pse.middleware.Broker;
import de.tum.cs.i1.pse.middleware.client.Client;

public abstract class AbstractClient implements Client {
	
	protected Broker broker;

	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	public abstract void callServer();
	
}
