package de.tum.cs.i1.pse.client.translation;

import de.tum.cs.i1.pse.client.AbstractClient;
import de.tum.cs.i1.pse.service.Service;
import de.tum.cs.i1.pse.service.translation.TranslationService;

public class TranslationClient extends AbstractClient {
	
	private TranslationService translationService;
	private final String wordToTranslate = "Dog";

	@Override
	public void callServer() {
		translateWord(wordToTranslate, TranslationService.LANGUAGE_GERMAN);
		translateWord(wordToTranslate, TranslationService.LANGUAGE_FRENCH);
	}

	public String translateWord(String word, String language) {
		configureTranslator(language);
		System.out.println();
		System.out.println("Translating " + word + " to " + language);
		String translation = translationService.translate(word, language);
		System.out.println(word + " in " + language + " translates to: " + translation);
		return translation;
	}

	private void configureTranslator(String language) {
		String serviceName = null;
		
		if (language.equals(TranslationService.LANGUAGE_GERMAN)) {
			serviceName = TranslationService.SERVICE_NAME_TRANSLATION_GERMAN;
		} else if (language.equals(TranslationService.LANGUAGE_FRENCH)) {
			serviceName = TranslationService.SERVICE_NAME_TRANSLATION_FRENCH;
		}
		
		Service service = this.broker.getService(serviceName);
		if (service instanceof TranslationService) {
			this.translationService = (TranslationService) service;
		} else {
			throw new IllegalStateException("Broker returned incorrect proxy for service.");
		}
	}
	
}
