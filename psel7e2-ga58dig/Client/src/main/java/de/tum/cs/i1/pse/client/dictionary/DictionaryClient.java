package de.tum.cs.i1.pse.client.dictionary;

import de.tum.cs.i1.pse.client.AbstractClient;
import de.tum.cs.i1.pse.service.Service;
import de.tum.cs.i1.pse.service.dictionary.DictionaryService;

public class DictionaryClient extends AbstractClient {
	
	private DictionaryService dictionaryService;
	private final String wordToLookUp = "Dog";
	
	@Override
	public void callServer() {
		lookupWord(wordToLookUp);
	}
	
	public void lookupWord(String word){
		if (this.dictionaryService == null) {
			configureService();
		}
		
		System.out.println("Looking up " + word);
		String definition = this.dictionaryService.lookupWord(word);
		System.out.println(word + " is defined as follows:\n" + definition);
	}

	private void configureService() {
		Service service = this.broker.getService(DictionaryService.SERVICE_NAME_DICTIONARY);
		if (service instanceof DictionaryService) {
			this.dictionaryService = (DictionaryService) service;
		} else {
			throw new IllegalStateException("Broker returned incorrect proxy for service.");
		}
	}
	
}
