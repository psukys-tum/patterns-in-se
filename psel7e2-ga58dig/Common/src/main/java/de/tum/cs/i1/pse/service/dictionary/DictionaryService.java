package de.tum.cs.i1.pse.service.dictionary;

import de.tum.cs.i1.pse.service.Service;

public interface DictionaryService extends Service {

	public static final String SERVICE_NAME_DICTIONARY = "Dictionary service";

	public abstract String lookupWord(String word);

}