package de.tum.cs.i1.pse.service.translation;

import de.tum.cs.i1.pse.service.Service;

public interface TranslationService extends Service {
	
	public static final String LANGUAGE_GERMAN = "German";
	public static final String LANGUAGE_FRENCH = "French";
	public static final String SERVICE_NAME_TRANSLATION_GERMAN = "German service";
	public static final String SERVICE_NAME_TRANSLATION_FRENCH = "French service";


	public String translate(String word, String language);
	
}